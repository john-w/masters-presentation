%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- HOMOGENEOUS SPACES -- %%%%%%%%%%%
\subsection{Espaços Homogêneos}
\renewcommand\subsecname{Fundamentos}
{
\begin{fframe}
    {\Large \bfseries \subsecname}

    Principais características:

    \begin{itemize}
        \item<2-> Apenas o setor espacial goza das simetrias do espaço,
        \begin{itemize}
            \item<3-> Referencial síncrono
                \tikzmarknode[ref]{sync}{\ref{geodiff:syncFrame}};
            \item<3-> Condição de isometria 
                \tikzmarknode[ref]{isometry}{\ref{kil:isometry}};
\iftoggle{tikz}{
                \begin{tikzpicture} [remember picture,overlay]
                    \node<3->[expblock](syncblk) at (7,1.5)
                                       {
                                           $ds^2 = d\tau^2 - \zeta_{ij}(x;\tau) dx^i dx^j$
                                       };
                    \draw<3->[exparrow](syncblk) to[out=160,in=60] (sync);
                    \node<3->[expblock](isometryblk) at (8,0)
                                       {
                                           $ \zeta'(x')_{ij} = \zeta(x')_{ij}$
                                       };
                    \draw<3->[exparrow](isometryblk) to[out=180,in=30] (isometry);
                \end{tikzpicture}
}{}
        \end{itemize}
        
        \item<4-> Referencial localmente inercial (N-Tuplas);
        \begin{itemize}
            \item<4-> No caso 3-dimensional, chamamos de \alert{tríades}.
        \end{itemize}
\iftoggle{tikz}{
        \begin{tikzpicture} [remember picture,overlay]
            \node<5->[expblock] at (8,0) 
                     {$\zeta_{ij}(x) = \tetU{a}{i}(x) \tetU{b}{j}(x) \eta_{ab}$};
        \end{tikzpicture}
}{}
    \end{itemize}

    \visible<6->{
        Considerando tudo, conseguimos escrever a condição de isometria para um ponto
        $x'$ na vizinhança como
        \begin{align}
            \tetU{a}{i}(x') dx'^i = \tetU{a}{i}(x) dx^i
            \label{hom:formTriadTransformation}
            \,,
        \end{align}
    }

    \visible<7->{
        que implica em
        \begin{align}
            \dpar{x'^j}{x^i} = \tetU{a}{i}(x) \ \tetD{a}{j}(x')
            \label{hom:homo_x'_x}
            \..
        \end{align}

    }

\end{fframe}

\begin{fframe}
    Impondo condições de integrabilidade
    \begin{align}
        \dpar{^2 x'^j}{x^i \partial x^k} 
        = \dpar{^2 x'^j}{x^k \partial x^i}
        \label{hom:homoCondInteg}
        \,,
    \end{align}

    \visible<2->{
        chega-se a
        \begin{align}
              \left(
                  \dpar{ \tetU{f}{k} }{x^i}(x) - \dpar{ \tetU{f}{i} }{x^k }(x)
              \right) \tetD{c}{k}(x) \tetD{d}{i}(x)
              \equiv \tC[^f_c_d]
              \label{hom:homoStructConsts}
              \,,
        \end{align}
    }

    \visible<3->{
        onde $\tC[^f_{cd}] = \tC[^f_{dc}]$ são as \alert{constantes de estrutura} de uma
        álgebra de Lie associada.
    }

    \begin{itemize}
        \item<4-> Derivada de Lie no referencial localmente inercial se reduz à derivada
                  direcional:
        \begin{align}
            \tX[_a] := \tetD{a}{i} \tpartial[_i]
            \label{hom:homoLieGenerator}
            \..
        \end{align}
        \begin{itemize}
            \item<5-> Derivada escalar de Lie;
            \item<6-> Gerador de translações;
        \end{itemize}

    \end{itemize}

\end{fframe}

\begin{fframe}
    \begin{itemize}
        \item Comutador da derivada escalar de Lie:
        \visible<2->{
        \begin{align}
            [\tX[_a], \tX[_b]]
            &= \tC[^d_a_b] \tX[_d]
            \label{hom:homoComutator}
        \end{align}
        }
        \begin{itemize}
            \item<3-> De fato satisfaz uma álgebra de Lie!
        \end{itemize}
    \end{itemize}

    \visible<4->{
        Os cálculos procedem mais facilmente se trabalharmos com o dual das constantes de
        estrutura
    }

    \vspace{-.5cm}
    \visible<5->{
    \begin{align}
        \tC[^c_a_b] := \tikzmarknode{eps}{\tvarepsilon[_{abd}]} \tC[^{dc}]
        \label{hom:homoConstDual}
        \,,
\iftoggle{tikz}{
        \begin{tikzpicture} [remember picture,overlay]
            \node<6>[brkblock](epsblk) at (2,1) 
                              {Pseudo-tensor totalmente anti-simétrico 
                               ($\varepsilon_{123} := 1$)};
            \draw<6>[exparrow](epsblk) to[in=60,out=-90] (eps);
        \end{tikzpicture}
}{}
    \end{align}
    }

    \visible<7->{
        já que dessa forma podemos decompô-lo em
        \begin{align}
            \tC[^{ab}] = \tikzmarknode{n}{\tn[^{ab}]} 
                       + \tikzmarknode{a}{\tvarepsilon[^{abc}] \ta[_c]}
            \label{hom:homoConstDualSeparated}
            \,,
\iftoggle{tikz}{
            \begin{tikzpicture} [remember picture,overlay]
                \node<8>[expblock](nblk) at (-3,-1.3)
                                  {Parte simétrica};
                \node<9>[expblock](ablk) at (1.4,-.7)
                                  {Parte anti-simétrica};
                \draw<8>[exparrow](nblk) to[out=90,in=-120] (n);
                \draw<9>[exparrow](ablk) to[out=90,in=-30] (a);
            \end{tikzpicture}
}{}
        \end{align}

        onde $a_c$ é um vetor arbitrário.
    }
    
\end{fframe}

\begin{fframe}
    Como as constantes provém de uma álgebra de Lie, elas têm que satisfazer a identidade
    de Jacobi

    \begin{align}
        \tC[^d_a_b] \tC[^f_d_c] +
        \tC[^d_b_c] \tC[^f_d_a] +
        \tC[^d_c_a] \tC[^f_d_b]
        &= 0
        \label{hom:homoJacobiIdentity}
        \,,
    \end{align}

    \visible<2->{
        implicando na equação secular

        \begin{align}
            \tn[^{ab}] \ta[_b] = 0
            \label{hom:homoEigenEq}
            \,,
        \end{align}

        que nos permite determinar $\tC[^c_{ab}]$ completamente.
    }
\end{fframe}

\begin{fframe}
    Como as tríades não são únicas, i.e., 
    $\quad \exists A^a_b \,;\ \tetU{a}{i} = A^a_b \tetU{b}{i}$%
    %
    \visible<2->{%
        , podemos escolhê-las de modo que $n^{ab}$ seja diagonal, tal que $a_c$
        corresponde aos autovalores nulos da equação.
    }

    \visible<3->{
    Escolhendo $a_c = (a, 0, 0)$ e definindo $n^{(i)} := n^{ii}$, sem perda de
    generalidade, encontramos duas classes de solução:
    }

    \begin{itemize}
        \item<4-> Classe A: $a=0$;
        \item<5-> Classe B: $n^{(1)} = 0$.
    \end{itemize}

    \visible<6->{Relações de comutação:}
    
    \vspace{-.5cm}
    \visible<7->{
    \begin{align}
        \begin{split}
        [ \tX[_1], \tX[_2] ]
        &= - a \tX[_2] + \tn[^{(3)}] \tX[_3]
        \,, \\
        [ \tX[_2], \tX[_3] ]
        &= \tn[^{(1)}] \tX[_1]
        \,, \\
        [ \tX[_3], \tX[_1] ]
        &= \tn[^{(2)}] \tX[_2] + a \tX[_3]
        \..
        \end{split}
        \label{hom:homoBianchiComutators}
    \end{align}
    }

\end{fframe}

\begin{fframe}
    Hão apenas \alert{nove} álgebras distintas que podem ser formadas de acordo com todas 
    as combinações de componentes nulas/não-nulas de $n$ e de suas orientações

    \visible<2->{
    \begin{table}[h!]
        \centering
        \begin{tabular}{clcccc}
            \hline
            Class & Type    &   $a$     &   $n^{(1)}$   &   $n^{(2)}$   &   $n^{(3)}$     \\
            \hline
            \mr{6}{A}   & I &   0       &   0           &   0           &   0             \\
            &II             &   0       &   1           &   0           &   0             \\
            &VII${}_0$      &   0       &   1           &   1           &   0             \\
            &VI${}_0$       &   0       &   1           &  -1           &   0             \\
            &IX             &   0       &   1           &   1           &   1             \\
            &VIII           &   0       &   1           &   1           &  -1             \\
            \hline
            \mr{4}{B}   & V &   1       &   0           &   0           &   0             \\
            &IV             &   1       &   0           &   0           &   1             \\
            &VII${}_a$      &   $a$     &   0           &   1           &   1             \\
            &III ($a=1$) \ \ \rdelim\}{2}{0pt}[]    \
                            &\mr{2}{$a$}& \mr{2}{0}     &\mr{2}{1}      &\mr{2}{-1}       \\
            &VI${}_a$ ($a\ne 1$) \
                            &           &               &               &                 \\
            \hline
        \end{tabular}
        \label{tab:hom:bianchi}
    \end{table}
    
    todas normalizadas pois sempre podemos re-escalar as tríades à unidade.%
    }
    \visible<2->{
    Bianchi\visible<2->{\footfullcite{ART:Bianchi2001GReGr..33.2171B}} foi pioneiro na 
    classificação, por isso esta recebe o nome de \alert{Classificação de Bianchi}.}
\end{fframe}

\iffalse
\begin{fframe}
    O tensor de Ricci, projetado sob as tríades e expresso pelas constantes de estrutura, 
    é dado por

    \vspace{-.5cm}
    \visible<2->{
    \begin{subequations}
    \begin{align}
        \begin{split}
            \tR[_0^0]         &= \frac{1}{2} \tensor{\chi}{_{(a)}^{(a)}}
                               + \frac{1}{2} \tensor{\chi}{^{(a)}_{(b)}}
                                           \tensor{\chi}{^{(b)}_{(a)}}
        \end{split} 
        \label{hom:RicciTetrad00}
        \,, \\
        \begin{split}
            \tR[_{(a)}^0]     &= \frac{1}{2} \tchi[^{(c)}_{(d)}] \bigg(
                                 \tC[^d_c_a] - \delta_a^d\tC[^b_b_c]
                                 \bigg)
        \end{split} 
        \label{hom:RicciTetrada0}
        \,, \\
        \begin{split}
            \tR[_{(a)}^{(b)}] &= \tP[_{(a)}^{(b)}] + \frac{1}{2\sqrt{\zeta}}
                                 \tpartial[_t]( \sqrt{\zeta} \tchi[_{(a)}^{(b)}] )
        \end{split}
        \label{hom:RicciTetradab}
        \,,
    \end{align}
    \label{hom:RicciTetrad}
    \end{subequations}
    }

    \visible<3->{
        onde $\chi_{ab} := \dot{\zeta}_{ab}$ e
        \begin{align}
            \tP[_{(a)}^{(b)}] &= \frac{1}{2\zeta} \bigg[
                2 \tC[^b^d] \tC[_a_d] + \tC[^d^b] \tC[_a_d] + \tC[^b^d] \tC[_d_a] - \tC[^d_d] 
                \big( \tC[^b_a] + \tC[_a^b] \big)
                \nonumber \\
                &\qquad \qquad
                + \delta_a^b \big( \big(\tC[^d_d]\big)^2 - 2  \tC[^d^f] \tC[_d_f] \big)
            \bigg]
            \label{hom:RicciTetradSpatial}
            \..
        \end{align}
    }

\end{fframe}
\fi

\begin{fframe}
    Comentários:
    \begin{itemize}
        \item<2-> Isometrias correspondem a classes de equivalência (grupos de movimentos);
        \item<3-> Ação é dada por $X_a = \tetD{a}{i} \partial_i$, que são geradores de
                  translação;
        \begin{itemize}
            \item<3-> Simetria esperada para espaços homogêneos.
        \end{itemize}
        \item<4-> Setor espacial como um todo define uma 3-superfície de transitividade do
                  grupo;
    \end{itemize}

    \visible<5->{
    Se computados os vetores de Killing através de 
    \tikzmarknode[ref]{kil}{\ref{kil:infinitesimalTransform}}%
    }
\iftoggle{tikz}{
    \begin{tikzpicture} [remember picture,overlay]
        \node<6->[expblock](kilblk) at (3,1.6) {$ x' = x + \varepsilon \xi(x)$};
        \draw<6->[exparrow](kilblk) to[out=-90,in=60] (kil);
    \end{tikzpicture}%
}{}%
    \visible<8->{%
        chegamos a
        \begin{align*}
            \txi[^j_{,i}] = \xi^k \left( \tetU{a}{i} \tetD{a}{j}_{,k} \right)
            \,,
        \end{align*}
    }

    \visible<8->{
        que pode ser expresso como
        \begin{align*}
            \txi[^j_{;i}] = 0
        \end{align*}
    }
    \vspace{-.5cm}
    \begin{itemize}
        \item<9-> Vetores de Killing transportados paralelamente sob geodésicas;
        \item<9-> Reitera o caráter de isometrias locais da teoria;
        \item<10-> \alert{Também comporta simetria conforme}.
    \end{itemize}
\end{fframe}

% -- CONFORMAL SYMMETRY -- %
\input{slides/C3/conformal}
% ------------------------ %

\renewcommand\subsecname{Invariância conforme}
\begin{fframe}
    \begin{itemize}
        \item<2-> Invariância da classificação de Bianchi por re-escala das tríades sugere
                  invariância conforme
    \end{itemize}

    \visible<3->{
    Impusemos transformação conforme especial
    \tikzmarknode[ref]{con}{ \ref{con:specialConform}}
        \begin{align}
            \tzeta_{ij}(x) \rightarrow \Omega^2(x)\ \teta[_{ij}]
            \ \,; \quad \Omega^2(x) > 0
            \label{hom:conformMetric}
        \end{align}
    }
        
    \visible<4->{
    na condição de isometria
        \begin{align*}
            \zeta'_{ij}(x') &= \zeta_{ij}(x')
            \..
        \end{align*}
    }

\end{fframe}

\begin{fframe}
     \visible<1->{
        De forma totalmente análoga ao que fora feito, chega-se a
        \vspace{-.3cm}

        \begin{align}
            \dpar{x'^j}{x^i} 
            = \frac{\Omega^2(x)}{\Omega^2(x')} \tetU{a}{i}(x) \ \tetD{a}{j}(x')
            \label{hom:homoConformal_x'_x}
            \..
        \end{align}
    }

    \vspace{-.5cm}
    \visible<1->{
    \begin{align}
        \implies
           \frac{1}{\Omega^2}
           \bigg[
                \frac{\tpartial \tetU{a}{e}}{\tpartial x^f}
              - \frac{\tpartial \tetU{a}{f}}{\tpartial x^e}
              + \frac{2}{\Omega} 
              \bigg( 
                  \frac{\tpartial \Omega}{\tpartial x^f} \tetU{a}{e}
                - \frac{\tpartial \Omega}{\tpartial x^e} \tetU{a}{f}
              \bigg)
          \bigg] \tetD{b}{e} \tetD{c}{f}
          \equiv \tC[^a_b_c]
          \label{hom:conformStructConsts}
          \,,
    \end{align}
    }

    \visible<2->{
        donde definindo a derivada de Lie escalar conforme
        \begin{align}
            Y_a := \frac{1}{\Omega^2} \tetD{a}{m} \tpartial[_m]
            \label{hom:conformLieDerivativeOperator}
            \,,
        \end{align}
    }

    \visible<3->{
        verifica-se que as mesmas álgebras são satisfeitas
        \begin{align}
            [Y_a, Y_b] = \tC[^c_a_b] Y_c
            \label{hom:conformCommutator}
            \,,
        \end{align}
    }

    \visible<3->{
        válidas agora também para dilatações arbitrárias.
    }
   
\end{fframe}

\begin{fframe}
    Se calculados os vetores de Killing, mapeando o fator conforme numa exponencial
    $\Omega(x) = e^{\omega(x)}$ e expandindo em primeira ordem,

    \visible<2->{
    \begin{align*}
        \txi[^j_{;i}] = -2 \tikzmarknode[highlight]{bk}{\omega_{,k}} \txi^k \delta^j_i
        \,,
\iftoggle{tikz}{
        \begin{tikzpicture}[remember picture,overlay]
            \node<2>[expblock](bkblk) at (2,2) 
                    {Parâmetro $b_k$ da A.C.
                    \tikzmarknode[ref]{conAC}{ \ref{con:conformalKillingVectors}}
                    };
            \draw<2>[exparrow](bkblk) to[out=-90,in=90] (bk);
            \node<2>[expblock](conACblk) at (2.7,-1.1) 
                    {$2 b_\alpha x^\alpha x^\mu - b^\mu x^2$};
            \draw<2>[exparrow](conACblk) to[out=90,in=-90] (conAC);
        \end{tikzpicture}
}{}
    \end{align*}
    }

    \visible<3->{
    ou, por \tikzmarknode[ref]{dlamb}{ \ref{con:killingWaveEq}}
\iftoggle{tikz}{
    \begin{tikzpicture}[remember picture,overlay]
        \node<3>[expblock](dlambblk) at (.2,1.3)
                {$ \partial^2 \tepsilon_\rho = - 2 \omega_{,\rho}$};
        \draw<3>[exparrow](dlambblk) to[out=-90,in=60] (dlamb);
    \end{tikzpicture}
}{}

    \begin{align*}
        \txi[^j_{;i}] = (\partial^2\xi_k) \txi^k \delta^j_i
        \..
    \end{align*}
    }

    \begin{itemize}
        \item<4-> Deixam de serem transportados paralelamente sob geodésicas;
        \begin{itemize}
            \item<5-> Especie de ``aceleração inercial'';
        \end{itemize}
    \end{itemize}
\end{fframe}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- ------------------ -- %%%%%%%%%%%
